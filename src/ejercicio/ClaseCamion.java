package ejercicio;

public enum ClaseCamion {
	CAMIONMOTO, CAMIONCOCHE, CAMIONBICI;

	public static ClaseCamion obtenerCamion(String inicial) {
		ClaseCamion salida = null;
		if ("M".equals(inicial)) {
			salida = ClaseCamion.CAMIONMOTO;
		} else if ("C".equals(inicial)) {
			salida = ClaseCamion.CAMIONCOCHE;
		} else if ("B".equals(inicial)) {
			salida = ClaseCamion.CAMIONBICI;
		} else {
			System.out.println("no existe esta inicial");
		}
		return salida;
	}

}
