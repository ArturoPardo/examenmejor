package ejercicio;

public class App {
	public static void main(String[] args) {
		App.ejercicio1();

	}

	public static void ejercicio1() {
		Franquicia franquicia = new Franquicia("Franquicia1");
		System.out.println(franquicia.getNombre());

		// Creamos vehiculos

		Coches coche1 = new Coches("audi", "black", "db324324", 4000, 300);
		Coches coche2 = new Coches("mercedes", "white", "db324324", 4000, 300);
		Bicis bici1 = new Bicis("hb", "ciudad", 3, 4);
		Bicis bici2 = new Bicis("JOOM", "ciudad", 3, 4);

		Motos moto1 = new Motos("vespa", "black", "5656767", 4000, 300);
		Motos moto2 = new Motos("vespa", "black", "2432we7", 4000, 300);

		franquicia.subirCamionCoches(coche1);
		franquicia.subirCamionCoches(coche2);
		franquicia.subirCamionMotos(moto1);
		franquicia.subirCamionMotos(moto2);
		franquicia.subirCamionBicis(bici1);
		franquicia.subirCamionBicis(bici2);

		franquicia.cargadosPorVehiculos("coches");
		franquicia.cargadosPorVehiculos("motos");
		franquicia.cargadosPorVehiculos("bicis");

		franquicia.limpiarVehiculoBicis(bici1);

	}

}
