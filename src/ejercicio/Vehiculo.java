package ejercicio;

public abstract class Vehiculo {
	String marca;
	String modelo;
	String matricula;
	int cilindrada;
	int potencia;

	public Vehiculo(String marca, String modelo, String matricula, int cilindrada, int potencia) {
		this.marca = marca;
		this.modelo = modelo;
		this.matricula = matricula;
		this.cilindrada = cilindrada;
		this.potencia = potencia;
	}
	
	public String getMarca() {
		return marca;
	}

	public String getModelo() {
		return modelo;
	}

	public String getMatricula() {
		return matricula;
	}

	public int getCilindrada() {
		return cilindrada;
	}

	public int getPotencia() {
		return potencia;
	}

	

}
