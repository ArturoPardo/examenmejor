package ejercicio;

public class Franquicia {
	private String nombre;

	private Coches coche;
	private Motos moto;
	private Bicis bici;

	private Camion camion1;
	private Camion camion2;
	private Camion camion3;

	private Bicis[] elementosCamionbici;
	private Coches[] elementosCamioncoche;
	private Motos[] elementosCamionmoto;

	private int contadorDeBicis = 0;
	private int contadorDeMotos = 0;
	private int contadorDeCoches = 0;

	public Franquicia(String nombre) {
//creo camiones los inicializo a 10
		this.nombre = nombre;
		this.camion1 = new Camion("B");
		this.camion2 = new Camion("M");
		this.camion3 = new Camion("C");

		this.elementosCamionbici = new Bicis[10];
		this.elementosCamioncoche = new Coches[10];
		this.elementosCamionmoto = new Motos[10];

	}

//alta vehiculos
	public void altaCoche(String marca, String modelo, String matricula, int cilindrada, int potencia) {
		this.coche = new Coches(marca, matricula, matricula, potencia, potencia);
	}

	public void altaBici(String marca, String modelo, int numeroPlatos, int numeroPinyones) {
		this.bici = new Bicis(modelo, modelo, numeroPinyones, numeroPinyones);
	}

	public void altaMotos(String marca, String modelo, String matricula, int cilindrada, int potencia) {
		this.moto = new Motos(matricula, matricula, matricula, potencia, potencia);
	}

	// subir camion

	public void subirCamionCoches(Coches coche) {

		this.elementosCamioncoche[this.contadorDeCoches] = coche;
		this.contadorDeCoches++;

	}

	public void subirCamionBicis(Bicis bici) {

		this.elementosCamionbici[this.contadorDeBicis] = bici;
		this.contadorDeBicis++;

	}

	public void subirCamionMotos(Motos moto) {

		this.elementosCamionmoto[this.contadorDeMotos] = moto;
		this.contadorDeMotos++;
	

	}
	// limpiar vehiculos
	public void limpiarVehiculoBicis(Bicis bici) {
		bici.estadoVehiculo();

		for (int i = 0; i < contadorDeBicis; i++) {

			if (elementosCamionbici[i].getMarca() == bici.getMarca()) {
				contadorDeBicis--;
				elementosCamionbici[i] = null;

			}

			else {
				System.out.println("no existe este vehiculo");

			}

		}

	}

	public void limpiarVehiculoBicis(Coches coche) {
		coche.estadoVehiculo();

		for (int i = 0; i < contadorDeCoches; i++) {

			if (elementosCamioncoche[i].getMarca() == coche.getMarca()) {
				contadorDeCoches--;
				elementosCamioncoche[i] = null;

			}

			else {
				System.out.println("no existe este vehiculo");

			}

		}
	}

	public void limpiarVehiculoMotos(Motos moto) {
		moto.estadoVehiculo();

		for (int i = 0; i < contadorDeMotos; i++) {
			if (elementosCamionmoto[i].getMarca() == moto.getMarca()) {

				contadorDeMotos--;
				System.out.println("Numero de motos ahora " + contadorDeMotos);
			} else {
				System.out.println("no existe este vehiculo");

			}
		}
	}
//muestra cargados
	public void cargadosPorVehiculos(String _tipo) {

		if (_tipo == "coches") {
			System.out.println("Vehiculos Guardados en el camion para coches :");
			for (int i = 0; i < contadorDeCoches; i++) {
				if (elementosCamioncoche[i].getMarca() != null) {
					System.out.println("matricula: " + elementosCamioncoche[i].getMatricula());
				}

			}

		} else if (_tipo == "motos") {
			System.out.println("Vehiculos Guardados en el camion para moto :");
			for (int i = 0; i < contadorDeMotos; i++) {
				if (elementosCamionmoto[i].getMarca() != null) {
					System.out.println("matricula: " + elementosCamionmoto[i].getMatricula());
				}

			}

		} else if (_tipo == "bicis") {
			System.out.println("Vehiculos Guardados en el camion para bicis :");
			for (int i = 0; i < contadorDeBicis; i++) {

				if (elementosCamionbici[i].getMarca() != null) {
					System.out.println("marca: " + elementosCamionbici[i].getMarca());
				}

				else {
					System.out.println("no existe este tipo");

				}
			}
		}
	}

	

	public Camion getCamion1() {
		return camion1;
	}

	public Camion getCamion2() {
		return camion2;
	}

	public Camion getCamion3() {
		return camion3;
	}


	public String getNombre() {
		return nombre;
	}

}
