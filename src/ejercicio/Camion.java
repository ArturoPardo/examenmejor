package ejercicio;

public class Camion {

	private ClaseCamion name;
	private String[] elementosCamion;
	private int contador;

	public Camion(String _name) {
		ClaseCamion name = ClaseCamion.obtenerCamion(_name);
		this.contador = 0;

	}
	public void cargarVehiculos(String _matricula){
		this.elementosCamion[this.contador] = _matricula;
		this.contador++;
	}

	
	public ClaseCamion getName() {
		return name;
	}

	public String[] getElementosCamion() {
		return elementosCamion;
	}
}
