package ejercicio;

public class VehiculoSinMotor {
	String marca;
	String modelo;

	public VehiculoSinMotor(String marca, String modelo, int numeroPlatos, int numeroPinyones) {
    
		this.marca = marca;
		this.modelo = modelo;
		this.numeroPlatos = numeroPlatos;
		this.numeroPinyones = numeroPinyones;
	}

	int numeroPlatos;
	int numeroPinyones;

	public String getMarca() {
		return marca;
	}

	public String getModelo() {
		return modelo;
	}

	public int getNumeroPlatos() {
		return numeroPlatos;
	}

	public int getNumeroPinyones() {
		return numeroPinyones;
	}

}
